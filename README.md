# LibShare Frontend

## Description

Project is currently a Work In Progress. This is the LibShare web application
front-end. It is meant to work alongside all other LibShare parts. It needs at
least a LibShare REST API to call, which is embarked into
[LibShare Backend](https://gitlab.com/libshare/libshare). This project is made
using [deno](https://deno.land/), [vue](https://vuejs.org/), other dependencies
are availabe [here](#Dependencies).

For more information see
[LibShare frontend full documentation](https://libshare.gitlab.io/libshare_front/).

## Project status

Currently work in progress, project is available
[here](https://gitlab.com/libshare/libshare). Project updates and devs are slow.

## Badges

**TODO**

## Visuals

**TODO**

## Installation

**TODO**

## Usage

**TODO**

## Support

**TODO**

## Roadmap

**TODO**

## Contributing

**TODO**

## Authors and acknowledgment

**TODO**

## License

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Dependencies

- [deno](https://deno.land/): JavaScript runtime, bundler
- [vue](https://vuejs.org/): Component-based programming framework
- [documentationjs](https://github.com/documentationjs/documentation/blob/master/docs/GETTING_STARTED.md):
  Documentation generator
