import { template } from './ContactList.template.js';

/**
 * Contact list view
 */
export const ContactList = {
  name: 'ContactList',
  template,
  data() {
    return {
      /**
       * List of contact
       *
       * @memberof ContactList
       */
      contactList: [],
      /**
       * Contact information list
       *
       * - first_name: contact first name
       * - last_name: contact last name
       * - email: contact email
       * - phone: contact phone number
       * - information: contact additional information
       * @memberof ContactList
       */
      columns: ['first_name', 'last_name', 'email', 'phone', 'information'],
      showDetails,
    };
  },
  mounted: getContactList,
};

/**
 * Show Contact details
 *
 * @param {Object} contact
 * @memberof ContactList
 */
export function showDetails(contact) {
  let route = { name: 'newContact' };
  if (contact.id) {
    route = {
      name: 'ContactDetails',
      params: { id: contact.id },
    };
  }
  this.$router.push(route);
}

/**
 * Get the user contact list
 * @memberof ContactList
 */
export async function getContactList() {
  const header = {
    'Content-Type': 'application/json',
  };
  const token = localStorage.getItem('token');

  try {
    if (token) {
      header.Authorization = token;
    } else {
      throw new Error('No token for current user.');
    }
    // eslint-disable-next-line no-undef
    const response = await fetch(`${_BACK_HOST}/contact`, {
      method: 'GET',
      mode: 'cors',
      headers: header,
    });
    this.contactList = await response.json();
  } catch (error) {
    console.error(error);
    this.$router.push({ name: 'Login' });
  }
}
