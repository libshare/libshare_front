import { html } from '../../scripts/utils/templating.js';

export const template = html`
  <div>
    <table>
      <thead>
        <tr>
          <th v-for="(column, idx) in columns" v-bind:key="idx">
            {{ column.toUpperCase() }}
          </th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="(contact, idx) in contactList" v-bind:key="idx" v-on:click="showDetails(contact)">
          <td v-for="(column, idx2) in columns" v-bind:key="idx2">
            <input type="checkbox" disabled v-if="column == 'shared'" :checked="contact.shared_id"/>
            <span v-else>{{ contact[column] }}</span>
          </td>
        </tr>
      </tbody>
    </table>
    <button v-on:click="showDetails({})" id="addContact" type="button">
      Add contact
    </button>
  </div>`;
