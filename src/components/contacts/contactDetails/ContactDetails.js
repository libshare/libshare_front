import { template } from './ContactDetails.template.js';
import { ShareModule } from '../shareModule/shareModule.js';

/**
 * Contact detailed information
 */
export const ContactDetails = {
  name: 'ContactDetails',
  components: { ShareModule },
  template,
  data() {
    return {
      /**
       * Contact id
       *
       * @memberof ContactDetails
       */
      shared_id: this.$route.params.id,
    };
  },
};
