import { html } from '../../../scripts/utils/templating.js';

export const template = html`
  <div class="contactDetails">
    <share-module :shared_id="shared_id" :show_buttons="true" :edit="true" :contact.sync="{}"></share-module>
  </div>`;
