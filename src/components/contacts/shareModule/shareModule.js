import { template } from './shareModule.template.js';

/**
 * Shared information
 */
export const ShareModule = {
  name: 'ShareModule',
  template,
  components: {},
  props: ['shared_id', 'edit', 'show_buttons', 'search'],
  data() {
    return {
      /**
       * Query for finding contact
       *
       * Query can be on any Contact information fields
       * @memberof ShareModule
       */
      query: '',
      /**
       * Contact informations
       *
       * - first_name: first name
       * - last_name: last name
       * - email: email
       * - phone: phone number
       * - information: additional information
       * @memberof ShareModule
       */
      contact: {
        first_name: '',
        last_name: '',
        email: '',
        phone: '',
        information: '',
      },
      saveContact,
      deleteContact,
    };
  },
  mounted: getContact,
  watch: {
    shared_id: function (newVal, oldVal) {
      this.user_id = newVal || oldVal;
      getContact.call(this);
    },
  },
  methods: {
    delayQueryContact,
    delayRemoveList,
    removeList,
    getContact,
    queryContact,
    removeContact,
  },
};

/**
 * Get contact
 * @param {Event} event
 * @memberof ShareModule
 */
export async function getContact(event) {
  const contactId = this.shared_id || event?.target?.id;
  if (!contactId) {
    return;
  }
  const header = {
    'Content-Type': 'application/json',
  };
  const token = localStorage.getItem('token');
  try {
    if (token) {
      header.Authorization = token;
    } else {
      throw new Error('No token for current user.');
    }
    const response = await fetch(
      // eslint-disable-next-line no-undef
      `${_BACK_HOST}/contact/${contactId}`,
      {
        mode: 'cors',
        method: 'GET',
        headers: header,
      },
    );
    this.contact = await response.json();
    this.delayRemoveList();
  } catch (error) {
    console.error(error);
    this.$router.push({ name: 'Login' });
  }
}

/**
 * Delete a contact in database
 * @memberof ShareModule
 */
export async function deleteContact() {
  try {
    const response = await fetch(
      // eslint-disable-next-line no-undef
      `${_BACK_HOST}/deleteContact/${this.contact.id}`,
      {
        mode: 'cors',
        method: 'DELETE',
        headers: {
          Authorization: localStorage.getItem('token'),
        },
      },
    );
    console.debug(await response.text());
    this.$router.push({ name: 'ContactList' });
  } catch (error) {
    console.error(error);
  }
}

/**
 * Remove contact selection
 * @memberof ShareModule
 */
export function removeContact() {
  this.contact = {
    first_name: '',
    last_name: '',
    email: '',
    phone: '',
    information: '',
  };
}

/**
 * Add a contact to database
 * @memberof ShareModule
 */
export async function saveContact() {
  let url = 'addContact';
  let method = 'PUT';
  if (this.contact.id) {
    method = 'POST';
    url = 'updateContact';
  }
  try {
    const response = await fetch(`${_BACK_HOST}/${url}`, {
      method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: localStorage.getItem('token'),
      },
      body: JSON.stringify(this.contact),
      mode: 'cors',
    });
    this.contact = await response.json();
    this.$router.push({ name: 'ContactList' });
  } catch (error) {
    console.error(error);
  }
}

/**
 * Query contacts to show them on hmi
 * @param {Event} event
 * @memberof ShareModule
 */
export async function queryContact(event) {
  const inputText = event.target.value;
  const url = new URL('contact', _BACK_HOST);
  try {
    const searchInput = this.removeList();
    if (inputText == '') {
      return;
    }
    url.searchParams.append('search', inputText);
    const response = await fetch(url, {
      method: 'GET',
      headers: {
        Authorization: localStorage.getItem('token'),
      },
    });
    const data = await response.json();
    if (data && data.length > 0) { // if items create a list of it
      const searchResults = document.createElement('div');
      searchResults.innerHTML = '';
      searchResults.classList = ['searchResults'];
      data.forEach((item) => {
        const result = document.createElement('div');
        result.innerText =
          `${item.first_name}, ${item.last_name}, ${item.email}, ${item.phone}`;
        result.title = item.information;
        result.id = item.id;
        result.onclick = this.getContact;
        searchResults.appendChild(result);
      });
      searchInput.appendChild(searchResults);
    }
  } catch (error) {
    console.error(error.message);
    console.debug(error);
  }
}

/**
 * Set a small timeout before executing query
 * @param {Event} event
 * @memberof ShareModule
 */
export function delayQueryContact(event) {
  setTimeout(() => this.queryContact.call(this, event), 250);
}

/**
 * Set a small timeout before removing list to let the click on element of list to work
 * @memberof ShareModule
 */
export function delayRemoveList() {
  setTimeout(() => this.removeList.call(this), 250);
}

/**
 * Remove the list of searched contact
 * @memberof ShareModule
 */
export function removeList() {
  try {
    const searchInput = document.getElementById('searchContact');
    const searchResults =
      document.querySelectorAll('#searchContact .searchResults') || [];
    searchResults.forEach((el) => searchInput.removeChild(el));
    return searchInput;
  } catch (error) {
    console.error(error.message);
    console.debug(error);
  }
}
