import { html } from '../../../scripts/utils/templating.js';

export const template = html`
    <style>
        .shareModule input {
            margin: 5px;
        }
    </style>
    <details class="shareModule" :open="edit">
        <summary v-if="show_buttons">Contact:</summary>
        <summary v-else>Shared with:
            <span>&nbsp;{{ contact.first_name }}</span>
            <span>&nbsp;{{ contact.last_name }}</span>
        </summary>
        <div v-if="search" class="formRow">
            <label for="searchContact">First name: </label>
            <div class="dataValue" id="searchContact" style="margin:0;margin-right:inherit;" >
                <input type="text" name="searchContact"  v-on:keyup="delayQueryContact" v-model="query" style="width:100%;margin:0;" :onblur="delayRemoveList"/>
            </div>
        </div>
        <form action="" method="POST" class="form-example" :style="show_buttons ? {} : {'border-left': '2px solid var(--main-color)'}">
            <div class="formRow">
                <label for="first_name">First name: </label>
                <input v-if="edit" type="text" class="dataValue" name="first_name" id="first_name" v-model="contact.first_name" />
                <span v-else class="dataValue" name="first_name" id="first_name">{{ contact.first_name }}</span>
            </div>
            <div class="formRow">
                <label for="last_name">Last name: </label>
                <input v-if="edit" type="text" class="dataValue" name="last_name" id="last_name" v-model="contact.last_name" />
                <span v-else class="dataValue" name="last_name" id="last_name">{{ contact.last_name }}</span>
            </div>
            <div class="formRow">
                <label for="email">Email: </label>
                <input v-if="edit" type="text" class="dataValue" name="email" id="email" v-model="contact.email" />
                <span v-else class="dataValue" name="email" id="email">{{ contact.email }}</span>
            </div>
            <div class="formRow">
                <label for="phone">Phone: </label>
                <input v-if="edit" type="text" class="dataValue" name="phone" id="phone" v-model="contact.phone" />
                <span v-else class="dataValue" name="phone" id="phone">{{ contact.phone }}</span>
            </div>
            <div class="formRow">
                <label for="information">Information: </label>
                <input v-if="edit" type="text" class="dataValue" name="information" id="information" v-model="contact.information" />
                <span v-else class="dataValue" name="information" id="information">{{ contact.information }}</span>
            </div>
            <div v-if="search||show_buttons" style="display: flex; justify-content: flex-end;">
                <input v-if="show_buttons" class="action" type="button" @click="saveContact()" value="save"/>
                <input v-if="show_buttons" class="danger" type="button" @click="deleteContact()" value="delete"/>
                <input v-if="search" class="danger" type="button" @click="removeContact()" value="remove"/>
            </div>
        </form>
    </details>
    `;
