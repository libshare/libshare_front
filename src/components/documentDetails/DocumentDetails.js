import { template } from './DocumentDetails.template.js';
import { ShareModule } from '../contacts/shareModule/shareModule.js';

/**
 * Document details information
 */
export const DocumentDetails = {
  name: 'DocumentDetails',
  components: { ShareModule },
  template,
  data() {
    return {
      /**
       * Document information
       *
       * - id: document unique id
       * - isbn: document ISBN (if any)
       * - title: document title
       * - authors: document authors
       * - mediaabstract: document abstration/description
       * - cover: document related image (URL)
       * - numberofmedia: number of document copies the user have
       * - shared_id: contact id if document is shared
       * @memberof DocumentDetails
       */
      documentDetails: {
        id: '',
        isbn: '',
        title: '',
        authors: '',
        mediaabstract: '',
        cover: '',
        numberofmedia: 0,
        shared_id: undefined,
      },
      editDocument,
      deleteDocument,
    };
  },
  mounted: getMediaInfo,
};

/**
 * Edit the current media
 *
 * Redirect to media edition page
 * @memberof DocumentDetails
 */
export function editDocument() {
  this.$router.push({
    name: 'PutDocument',
    params: { id: this.documentDetails.id },
  });
}

/**
 * Delete current media
 *
 * Remove current media based on its ID
 * @async
 * @memberof DocumentDetails
 */
export async function deleteDocument() {
  try {
    const response = await fetch(
      // eslint-disable-next-line no-undef
      `${_BACK_HOST}/delete/${this.documentDetails.id}`,
      {
        mode: 'cors',
        method: 'DELETE',
        headers: {
          Authorization: localStorage.getItem('token'),
        },
      },
    );
    console.debug(await response.text());
    this.$router.push({ name: 'DocumentList' });
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get media information
 * @async
 * @memberof DocumentDetails
 */
export async function getMediaInfo() {
  const header = {
    'Content-Type': 'application/json',
  };
  const token = localStorage.getItem('token');
  try {
    if (token) {
      header.Authorization = token;
    } else {
      throw new Error('No token for current user.');
    }
    const response = await fetch(
      // eslint-disable-next-line no-undef
      `${_BACK_HOST}/documents/${this.$route.params.id}`,
      {
        mode: 'cors',
        method: 'GET',
        headers: header,
      },
    );
    this.documentDetails = await response.json();
  } catch (error) {
    console.error(error);
    this.$router.push({ name: 'Login' });
  }
}
