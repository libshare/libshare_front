import { html } from '../../scripts/utils/templating.js';

export const template = html`
  <div id="DocumentDetails">
    <div id="UpdateButtons">
      <button v-on:click="editDocument()" id="EditDocument" type="button">
        Edit document
      </button>
      <button v-on:click="deleteDocument()" id="DeleteDocument" type="button">
        Delete document
      </button>
    </div>
    <div id="MainInformation">
      <fieldset>
        <legend v-if="documentDetails.isbn" style="font-weight: bold">{{ documentDetails.isbn }}</legend>
        <div class="formRow">
          <label for="title">Title: </label>
          <span class="dataValue" id="title">{{ documentDetails.title }}</span>
        </div>
        <div class="formRow">
          <label for="authors">Authors: </label>
          <span class="dataValue" id="authors">{{ documentDetails.authors }}</span>
        </div>
        <div class="formRow">
          <label for="numberofmedia">Number of documents: </label>
          <span class="dataValue" id="numberofmedia">{{ documentDetails.numberofmedia }}</span>
        </div>
        <div class="formRow">
          <label for="mediaabstract">Abstract: </label>
          <span class="dataValue" id="mediaabstract"
          style="white-space: pre-wrap">{{ documentDetails.mediaabstract }}</span>
        </div>
        <share-module v-if="documentDetails.shared_id" :shared_id="documentDetails.shared_id" ></share-module>
      </fieldset>
    </div>
    <div id="Cover">
      <img id="CoverImage" :src="documentDetails.cover" :title="documentDetails.title + ' cover'"
        :alt="documentDetails.title + ' cover'" />
    </div>
  </div>`;
