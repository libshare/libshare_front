import { template } from './DocumentList.template.js';

/**
 * Document list
 *
 * List of all user documents
 */
export const DocumentList = {
  name: 'DocumentList',
  template,
  data() {
    return {
      /**
       * List of documents to display
       * @memberof DocumentList
       */
      documentList: [],
      /**
       * Documents information to display
       *
       * - isbn: isbn (if any)
       * - title: document title
       * - authors: document authors
       * - shared: true if document is shared
       * @memberof DocumentList
       */
      columns: ['isbn', 'title', 'authors', 'shared'],
      showDetails,
    };
  },
  mounted: getMedias,
};

/**
 * Show document details
 *
 * @param {Object} document
 * @memberof DocumentList
 */
export function showDetails(document) {
  this.$router.push({
    name: 'DocumentDetails',
    params: { id: document.id },
  });
}

/**
 * Get the list of medias for current user
 * @memberof DocumentList
 */
export async function getMedias() {
  const header = {
    'Content-Type': 'application/json',
  };
  const token = localStorage.getItem('token');

  try {
    if (token) {
      header.Authorization = token;
    } else {
      throw new Error('No token for current user.');
    }
    // eslint-disable-next-line no-undef
    const response = await fetch(`${_BACK_HOST}/documents`, {
      method: 'GET',
      mode: 'cors',
      headers: header,
    });
    this.documentList = await response.json();
  } catch (error) {
    console.error(error);
    this.$router.push({ name: 'Login' });
  }
}
