import { html } from '../../scripts/utils/templating.js';

export const template = html`
  <div>
    <table>
      <thead>
        <tr>
          <th v-for="(column, idx) in columns" v-bind:key="idx">
            {{ column.toUpperCase() }}
          </th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="(document, idx) in documentList" v-bind:key="idx" v-on:click="showDetails(document)">
          <td v-for="(column, idx2) in columns" v-bind:key="idx2">
            <input type="checkbox" disabled v-if="column == 'shared'" :checked="document.shared_id"/>
            <span v-else>{{ document[column] }}</span>
          </td>
        </tr>
      </tbody>
    </table>
  </div>`;
