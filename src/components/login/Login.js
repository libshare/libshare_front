import { template } from './Login.template.js';
import { toggleVisibility } from '../../scripts/utils/templating.js';

/**
 * User log in page
 */
export const Login = {
  name: 'Login',
  template,
  data() {
    return {
      /**
       * User information
       *
       * - email: user email
       * - password: user password
       * @memberof Login
       */
      user: {
        email: '',
        password: '',
      },
      login,
      toggleVisibility,
    };
  },
  mounted() {
    localStorage.removeItem('token');
  },
  methods: {
    handleSubmit() {
      this.login();
    }
  }
};

/**
 * Login function.
 *
 * Authenticate user on back-office according to user info.
 *
 * **POST** on URL `/authenticate` with user information
 *
 * @returns a token if authentication successfull
 * @memberof Login
 */
export async function login() {
  if (!this.user || !this.user.email || !this.user.password) {
    return;
  }
  try {
    const response = await fetch(
      // eslint-disable-next-line no-undef
      `${_BACK_HOST}/authenticate`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(this.user),
        mode: 'cors',
      },
    );
    const data = await response.json();
    setToken(this, data);
  } catch (error) {
    console.error(error);
  } finally {
    this.$store.commit('loggerTitle');
  }
}

/**
 * Register token to local storage
 * @param {Object} self context
 * @param {Response.data} data response data containing a token
 * @memberof Login
 */
export function setToken(self, data) {
  if (data && data.token) {
    localStorage.setItem('token', `Bearer ${data.token}`);
    self.$router.push({ name: 'DocumentList' });
  } else {
    throw new Error(`No token returned for user ${self.user.email}`);
  }
}
