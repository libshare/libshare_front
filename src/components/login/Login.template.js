import { html } from '../../scripts/utils/templating.js';

export const template = html`
  <div>
    <form @submit.prevent="handleSubmit">
      <fieldset>
        <legend>Log in</legend>
        <div class="formRow">
          <label for="email">Email: </label>
          <input type="email" pattern=".+@.+\..+" class="dataValue" id="email" required v-model="user.email" />
        </div>
        <div class="formRow">
          <label for="password">Password: </label>
          <input type="password" class="dataValue" id="password" required v-model="user.password" />
        </div>
        <div>
          <!-- An element to toggle between password visibility -->
          <input type="checkbox" v-on:click="toggleVisibility()" id="visibility" name="visibility" />
          <label for="visibility">Show Password</label>
        </div>
        <div class="formRow">
          <button type="submit" title="Log in">
            Log in
          </button>
        </div>
      </fieldset>
    </form>
  </div>`;
