import { template } from './NotFound.template.js';

/**
 * Not found page
 */
export const NotFound = {
  name: 'NotFound',
  template,
};
