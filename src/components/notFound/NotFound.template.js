import { html } from '../../scripts/utils/templating.js';

export const template = html`
  <div>
    <h1>404 Not found</h1>
    <p>
      The page you meant to reach is not available or you do not have right
      access to it.
    </p>
    <router-link to="/" title="Go back to main page">Go back to main page</router-link>
  </div>`;
