import { SearchInput } from '../searchInput/SearchInput.js';
import { ShareModule } from '../contacts/shareModule/shareModule.js';
import { template } from './PutDocument.template.js';

/**
 * Edit document information
 */
export const PutDocument = {
  name: 'PutDocument',
  components: { SearchInput, ShareModule },
  template,
  data() {
    return {
      /**
       * Document information
       *
       * - id: document unique id
       * - isbn: document ISBN (if any)
       * - title: document title
       * - authors: document authors
       * - mediaabstract: document abstration/description
       * - cover: document related image (URL)
       * - numberofmedia: number of document copies the user have
       * - shared_id: contact id if document is shared
       * @memberof PutDocument
       */
      documentDetails: {
        id: '',
        isbn: '',
        title: '',
        authors: '',
        mediaabstract: '',
        cover: '',
        numberofmedia: 0,
        shared_id: undefined,
      },
      saveDocument,
    };
  },
  methods: {
    updateBookInfo,
  },
  mounted: getMediaInfo,
};

/**
 * Update book information
 * @param {object} bookInfo Book information => {isbn: string, title: string, authors: string, abstract: string}
 * @memberof PutDocument
 */
export function updateBookInfo(bookInfo) {
  try {
    this.documentDetails.isbn = bookInfo.isbn;
    this.documentDetails.title = bookInfo.title;
    this.documentDetails.authors = bookInfo.authors;
    this.documentDetails.mediaabstract = bookInfo.abstract;
  } catch (error) {
    console.error(error.message);
    console.debug(error);
  }
}

/**
 * Get media information
 * @memberof PutDocument
 */
export async function getMediaInfo() {
  if (this.$route.params.id) {
    const header = {
      'Content-Type': 'application/json',
    };
    const token = localStorage.getItem('token');
    try {
      if (token) {
        header.Authorization = token;
      } else {
        throw new Error('No token for current user.');
      }
      const response = await fetch(
        // eslint-disable-next-line no-undef
        `${_BACK_HOST}/documents/${this.$route.params.id}`,
        {
          mode: 'cors',
          method: 'GET',
          headers: header,
        },
      );
      this.documentDetails = await response.json();
    } catch (error) {
      console.error(error);
      this.$router.push({ name: 'Login' });
    }
  }
}

/**
 * Save media information.
 *
 * @async
 * @memberof PutDocument
 */
export async function saveDocument() {
  this.documentDetails.shared_id = this.$refs.contactInformation.contact?.id;
  let url = 'addDocument';
  let method = 'PUT';
  if (this.documentDetails.id) {
    url = 'updateDocument';
    method = 'POST';
  }
  try {
    // eslint-disable-next-line no-undef
    const response = await fetch(`${_BACK_HOST}/${url}`, {
      method: method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: localStorage.getItem('token'),
      },
      body: JSON.stringify(this.documentDetails),
      mode: 'cors',
    });
    this.documentDetails = await response.json();
    this.$router.push({ name: 'DocumentList' });
  } catch (error) {
    console.error(error);
  }
}
