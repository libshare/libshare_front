import { html } from '../../scripts/utils/templating.js';

export const template = html`
  <div class="PutDocument">
    <fieldset>
      <legend>Add a new document to your library</legend>
      <div class="formRow">
        <label for="isbn">isbn: </label>
        <search-input @updateBookInfo="updateBookInfo" name="isbn" class="dataValue" :inputValue="documentDetails.isbn" isRequired="" queryType="isbn"></search-input>
      </div>
      <div class="formRow">
        <label for="title">Title: </label>
        <search-input @updateBookInfo="updateBookInfo" name="title" class="dataValue" :inputValue="documentDetails.title" isRequired="true" queryType="title"></search-input>
      </div>
      <div class="formRow">
        <label for="authors">Authors: </label>
        <search-input @updateBookInfo="updateBookInfo" name="authors" class="dataValue" :inputValue="documentDetails.authors" isRequired="true" queryType="authors"></search-input>
      </div>
      <div class="formRow">
        <label for="mediaabstract">Abstract: </label>
        <textarea class="dataValue" id="mediaabstract" name="mediaabstract"
          v-model="documentDetails.mediaabstract"></textarea>
      </div>
      <div class="formRow">
        <label for="cover">Cover link: </label>
        <input type="text" class="dataValue" id="cover" name="cover" v-model="documentDetails.cover" />
      </div>
      <div class="formRow">
        <label for="numberofmedia">Number of documents: </label>
        <input type="number" class="dataValue" id="numberofmedia" name="numberofmedia"
          v-model="documentDetails.numberofmedia" />
      </div>
      <share-module v-if="true" :shared_id="documentDetails.shared_id" :search="true" ref="contactInformation" :contact.sync="{}"></share-module>
      <div class="formRow">
        <button type="button" v-on:click="saveDocument()" title="Add or Update document to the database">
          Add or Update document!
        </button>
        <img :src="documentDetails.cover"
          :alt="documentDetails.cover ? documentDetails.title + ' cover' : 'Waiting for a cover to display ...'"
          :title="documentDetails.title + ' cover'" />
      </div>
    </fieldset>
  </div>`;
