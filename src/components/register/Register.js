import { toggleVisibility } from '../../scripts/utils/templating.js';
import { template } from './Register.template.js';

/**
 * User registration page
 */
export const Register = {
  name: 'Register',
  // eslint-disable-next-line no-undef
  template,
  data() {
    return {
      /**
       * User information
       *
       * - username: user pseudo
       * - email: user email
       * - password: user password
       * @memberof Register
       */
      user: {
        username: '',
        email: '',
        password: '',
      },
      register,
      toggleVisibility,
    };
  },
  mounted() {
    localStorage.removeItem('token');
  },
};

/**
 * Register the user
 * @memberof Register
 */
export async function register(self) {
  checkUserInformation(self.user);
  checkPassword(self.user);
  await registerUserToBackEnd(self, self.user);
}

/**
 * Check user information
 * @memberof Register
 */
export function checkUserInformation(user) {
  if (
    !user ||
    !user.username ||
    !user.email ||
    !user.password
  ) {
    console.error('Missing user information.');
  }
}

/**
 * Check user password
 * @memberof Register
 */
export function checkPassword(user) {
  if (user.password !== user.checkPassword) {
    console.error('Password and password confirmation are not the same.');
  }
}

/**
 * Register user to back end
 * @async
 * @param {any} user User to register to back end
 * @memberof Register
 */
export async function registerUserToBackEnd(self, user) {
  try {
    const response = await fetch(
      `${_BACK_HOST}/register`, // eslint-disable-line no-undef
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(user),
        mode: 'cors',
      },
    );
    const data = await response.json();
    if (data && data.token) {
      localStorage.setItem(
        'token',
        `Bearer ${data.token}`,
      );
      self.$router.push({ name: 'DocumentList' });
    } else {
      throw new Error(`No token returned for user ${user.email}`);
    }
  } catch (error) {
    console.error(error);
  } finally {
    self.$store.commit('loggerTitle');
  }
}
