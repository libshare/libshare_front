import { html } from '../../scripts/utils/templating.js';

export const template = html`
  <div>
    <fieldset>
      <legend>Register</legend>
      <div class="formRow">
        <label for="username">Username: </label>
        <input type="text" class="dataValue" id="username" required v-model="user.username" />
      </div>
      <div class="formRow">
        <label for="email">Email: </label>
        <input type="email" pattern=".+@.+\..+" class="dataValue" id="email" required v-model="user.email" />
      </div>
      <div class="formRow">
        <label for="password">Password: </label>
        <input type="password" class="dataValue" id="password" required v-model="user.password" />
      </div>
      <div>
        <!-- An element to toggle between password visibility -->
        <input type="checkbox" v-on:click="toggleVisibility()" id="visibility" name="visibility" />
        <label for="visibility">Show Password</label>
      </div>
      <div class="formRow">
        <label for="checkPassword">Password confirmation: </label>
        <input type="password" class="dataValue" id="checkPassword" required v-model="user.checkPassword" />
      </div>
      <div class="formRow">
        <button type="button" v-on:click="register(this)" title="Register">
          Register
        </button>
      </div>
    </fieldset>
  </div>`;
