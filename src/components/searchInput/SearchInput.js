import { ISBNError } from '../../scripts/errors/ISBNError.js';
import { InputError } from '../../scripts/errors/InputError.js';
import { template } from './SearchInput.template.js';

/**
 * Search document
 */
export const SearchInput = {
  name: 'SearchInput',
  template,
  props: ['inputValue', 'queryType', 'isRequired'],
  methods: {
    delayQueryBook,
    delayRemoveList,
    queryBook,
    removeList,
    sendInfo,
    getISBN13,
    sanitizeISBN,
  },
};

/**
 * Set a small timeout before executing query
 * @memberof SearchInput
 */
export function delayQueryBook(event) {
  setTimeout(() => this.queryBook.call(this, event), 250);
}

/**
 * Set a small timeout before removing list to let the click on element of list to work
 * @memberof SearchInput
 */
export function delayRemoveList() {
  setTimeout(() => this.removeList.call(this), 250);
}

/**
 * Remove search list results
 * @returns the list container
 * @memberof SearchInput
 */
export function removeList() {
  try {
    const searchInput = document.getElementById(this.queryType);
    const searchResults =
      document.querySelectorAll(`#${this.queryType} .searchResults`) || [];
    searchResults.forEach((el) => searchInput.removeChild(el));
    return searchInput;
  } catch (error) {
    console.error(error.message);
    console.debug(error);
  }
}

/**
 * Send book information to parent document
 * @param {Event} event contains target information
 * @memberof SearchInput
 */
export async function sendInfo(event) {
  try {
    this.removeList();
    const response = await fetch(
      `https://www.googleapis.com/books/v1/volumes/${event.target.id}`,
      {
        method: 'GET',
      },
    );
    const data = await response.json();
    const bookInfo = {
      isbn: this.getISBN13(data.volumeInfo.industryIdentifiers),
      title: data.volumeInfo.title,
      authors: data.volumeInfo.authors?.join(', '),
      abstract: data.volumeInfo.description,
    };
    this.$emit('updateBookInfo', bookInfo);
  } catch (error) {
    console.error(error.message);
    console.debug(error);
  }
}

/**
 * Query books list from Google API from queryType and input value
 * @param {Event} event contains target information
 * @memberof SearchInput
 */
export async function queryBook(event) {
  let inputText = event.target.value;
  try {
    const searchInput = this.removeList();
    if (inputText == '') {
      throw new InputError('Field is required');
    }
    if (this.queryType == 'isbn') {
      inputText = this.sanitizeISBN(inputText);
    }
    const response = await fetch(
      `https://www.googleapis.com/books/v1/volumes?q=${this.queryType}:${inputText}`,
      {
        method: 'GET',
      },
    );
    const data = await response.json();
    if (data && data.totalItems > 0) { // if items create a list of it
      const searchResults = document.createElement('div');
      searchResults.innerHTML = '';
      searchResults.classList = ['searchResults'];
      data.items.forEach((item) => {
        const result = document.createElement('div');
        result.innerText = `${
          this.getISBN13(item.volumeInfo.industryIdentifiers)
        }, ${item.volumeInfo.title}, ${item.volumeInfo.authors?.join(', ')}`;
        result.title = item.volumeInfo.description;
        result.id = item.id;
        result.onclick = this.sendInfo;
        searchResults.appendChild(result);
      });
      searchInput.appendChild(searchResults);
    }
  } catch (error) {
    console.error(error.message);
    console.debug(error);
  }
}

/**
 * Get ISBN 13
 * @param {Object[]} ISBNList an array of isbn
 * @returns the isbn of length 13
 * @memberof SearchInput
 */
export function getISBN13(ISBNList) {
  if (ISBNList) {
    const isbn13 = ISBNList.filter((isbn) => isbn.type == 'ISBN_13');
    return isbn13 && isbn13.length > 0 ? isbn13[0].identifier : '';
  } else {
    return '';
  }
}

/**
 * Sanitize provided ISBN
 * @param {String} isbn isbn to check
 * @returns formatted ISBN
 * @throws ISBNError when ISBN format is incorrect
 * @memberof SearchInput
 */
export function sanitizeISBN(isbn) {
  isbn = isbn.trim();
  if (/\p{L}/u.test(isbn)) { // any letter, even non ASCII https://stackoverflow.com/questions/3617797/regex-to-match-only-letters#comment117866996_3617818
    throw new ISBNError(
      'Wrong ISBN format, it should be a number of 10 or 13 digits long',
    );
  }
  isbn = isbn.replace(/\D/g, '');
  if (isbn.length != 10 && isbn.length != 13) {
    throw new ISBNError('Wrong ISBN format, it must be 10 or 13 digits long');
  }
  return isbn;
}
