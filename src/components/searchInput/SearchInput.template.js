import { html } from '../../scripts/utils/templating.js';

export const template = html`
    <div :id="queryType" style="margin:0;margin-right:inherit;">
        <input type="text" :required="!!isRequired" style="width:100%" :onkeyup="delayQueryBook" :value="inputValue"
            :onblur="delayRemoveList" />
    </div>`;
