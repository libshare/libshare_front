import { template } from './vt-table.template.js';
// @deno-types="https://unpkg.com/browse/vue@3.2.45/dist/vue.d.ts"
import * as Vue from '../externals/vue.js';

const emit = Vue.defineEmits(['rowData']);
const props = Vue.defineProps({
  data: {
    caption: String,
    head: [{ colspan: Number, text: String } | [String]],
    body: [Object],
    options: Object,
  },
});
let conf = {
  id: '',
  selectable: false,
};

if (props.data.options) {
  conf = Object.assign(conf, props.data.options);
}

function _returnObject(obj) {
  emit('rowData', obj);
}

export const vtTable = {
  name: 'vtTable',
  template,
  data() {
    return {
      caption: String,
      head: [{ colspan: Number, text: String } | [String]],
      body: [Object],
      options: Object,
    };
  },
};
