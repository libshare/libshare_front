import { html } from '../scripts/utils/templating.js';

export const template = html`
  <table>
    <caption v-if="data.caption">{{ data.caption }}</caption>
    <thead v-if="data.head" ref="toto">
        <tr>
            <th v-for="head in data.head" :colspan="head.colspan || 1">{{ head.text || head }}</th>
        </tr>
    </thead>
    <tbody v-if="data.body">
        <tr v-for="row in data.body" :data-id="row[conf.id]" :class="{ cursor: conf.selectable }" @click="conf.selectable ? _returnObject(row) : null">
            <td v-for="col in row">{{ col }}</td>
        </tr>
    </tbody>
  </table>
`;
