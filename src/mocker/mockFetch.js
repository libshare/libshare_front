globalThis.fetch = (...args) => {
  const url = trimUrl(args[0]);
  let response = '';
  const options = {
    status: 200,
  };
  switch (url) {
    case '/register':
    case '/authenticate':
      response = {
        token: 'toto',
      };
      break;
    case '/documents':
      response = [
        {
          id: 1,
          isbn: 1234567891,
          title: 'First book',
          authors: 'Myself, Me, Mine',
        },
        {
          id: 2,
          isbn: 1987654321,
          title: 'Second book',
          authors: 'Myself Again',
          shared_id: 1,
        },
      ];
      break;
    case '/documents/1':
    case '/documents/2':
      response = {
        id: 1,
        isbn: 1234567891,
        title: 'First book',
        authors: 'Myself, Me, Mine',
        numberofmedia: 3,
        mediaabstract: 'Don\'t expect too much from a mock.',
        cover: '/assets/favicon.ico',
        shared_id: 1,
      };
      break;
    case '/contact/1':
      response = {
        id: 1,
        first_name: 'Me',
        last_name: 'Myself',
        email: 'me.myself@libshare.local',
        phone: '123456789',
        information: 'Do not expect too much from a mock. (v2)',
      };
      break;
    case '/contact':
    case '/contact?':
      response = [{
        id: 1,
        first_name: 'Me',
        last_name: 'Myself',
        email: 'me.myself@libshare.local',
        phone: '123456789',
        information: 'Do not expect too much from a mock. (v2)',
      }];
      break;
    case '/delete/1':
    case '/updateDocument':
    case '/addDocument':
    case '/deleteContact/1':
    case '/updateContact':
    case '/addContact':
      break;
    default:
      console.error(`URI ${url} not found. Received:`);
      console.debug(args);
      options.status = 404;
  }
  return new Response(JSON.stringify(response), options);
};

/**
 * Trim an URL
 *
 * @param {string} url url to trim
 */
function trimUrl(url) {
  if (typeof url == 'object') {
    url = url.href;
  }
  return url.replace(`https://${location.hostname}`, '').replace(
    /\?.*/,
    '?',
  );
}
