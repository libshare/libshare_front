// @deno-types="https://unpkg.com/browse/vue@3.2.45/dist/vue.d.ts"
import * as Vue from './externals/vue.js';
// @deno-types="https://unpkg.com/vue-router@4.1.6/dist/vue-router.d.ts" />
import { VueRouter } from './externals/vue-router.js';
import { VueX } from './externals/vue-x.js';

import { DocumentList } from './components/documentList/DocumentList.js';
import { DocumentDetails } from './components/documentDetails/DocumentDetails.js';
import { ContactList } from './components/contacts/ContactList.js';
import { ContactDetails } from './components/contacts/contactDetails/ContactDetails.js';
import { PutDocument } from './components/putDocument/PutDocument.js';
import { Login } from './components/login/Login.js';
import { Register } from './components/register/Register.js';
import { NotFound } from './components/notFound/NotFound.js';
import { loggerTitle } from './scripts/utils/storageManagement.js';

globalThis.addEventListener('load', () => {
  registerSW();
});

/**
 * Check user authentication
 *
 * If user is authenticate, go to requested page,
 * else send user to login page
 * @param {*} _to
 * @param {*} _from
 * @param {function} next continue if user is logged in
 */
function checkAuth(_to, _from, next) {
  if (store.state.isLoggedIn) next();
  else '/login';
}

/**
 * Service worker registration
 *
 * If browser support service worker, then register it,
 * else log an error
 */
function registerSW() {
  if ('serviceWorker' in navigator) {
    try {
      navigator.serviceWorker.register('/service-worker.js');
    } catch (error) {
      console.error(`Error while registering service worker: ${error}`);
    }
  } else {
    console.error('Service worker can not be registered.');
  }
}

/**
 * Application routes
 *
 * @example {
 *    name: {string} route name,
 *    path: {string} route path as shown in URI,
 *    component: {object} component corresponding to the route
 * and which will be used,
 *    beforeEnter: {function} action to be called before enter component
 * }
 */
const routes = [
  {
    name: 'DocumentList',
    path: '/',
    component: DocumentList,
    beforeEnter: checkAuth,
  },
  {
    name: 'DocumentDetails',
    path: '/document/:id',
    component: DocumentDetails,
    beforeEnter: checkAuth,
  },
  {
    name: 'ContactList',
    path: '/contact',
    component: ContactList,
    beforeEnter: checkAuth,
  },
  {
    name: 'ContactDetails',
    path: '/contact/:id',
    component: ContactDetails,
    beforeEnter: checkAuth,
  },
  {
    name: 'newContact',
    path: '/contact',
    component: ContactDetails,
    beforeEnter: checkAuth,
  },
  {
    name: 'PutDocument',
    path: '/putDocument',
    component: PutDocument,
    beforeEnter: checkAuth,
  },
  { name: 'Login', path: '/login', component: Login },
  { name: 'Register', path: '/register', component: Register },
  { path: '/:pathMatch(.*)*', name: 'not-found', component: NotFound },
  { path: '/:pathMatch(.*)', name: 'bad-not-found', component: NotFound },
];

/** @type {VueRouter.Router} Router object */
const router = VueRouter.createRouter({
  history: VueRouter.createWebHashHistory(),
  base: _BASE_URL,
  routes,
});

/**
 * VueX store to allow variables to be shared and mutated anywhere
 */
const store = new VueX.createStore({
  state() {
    return { logger: 'Log in', isLoggedIn: true };
  },
  mutations: {
    loggerTitle(state) {
      state.logger = loggerTitle(state);
    },
    logout(state) {
      localStorage.removeItem('token');
      state.logger = loggerTitle(state);
    },
  },
});
/* eslint-enable no-undef */

/**
 * Main view
 *
 * @class
 */
export const Main = {
  data: function () {
    return {
      /**
       * Log out current User
       *
       * @memberof Main
       */
      logout: function () {
        this.$store.commit('logout');
      },
    };
  },
  computed: {
    logger() {
      return this.$store.state.logger;
    },
  },
  mounted: function () {
    this.$store.commit('loggerTitle');
    this.$router.push({ name: 'Login' });
  },
};

const app = Vue.createApp(Main);

app.use(router);
app.use(store);
app.config.globalProperties.$store = store;
app.mount('#app');
