/**
 * Raised when ISBN format is incorrect
 */
export class ISBNError extends Error {
  constructor(message) {
    super(message);
    this.name = 'ISBNError';
  }
}
