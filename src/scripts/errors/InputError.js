/**
 * Raised when user has made an input error
 *
 * Can be a missing information, a typing error or a wrong input format
 *
 * .. important:: A custom message must be added to provide further information
 *    on the error.
 */
export class InputError extends Error {
  constructor(message) {
    super(message);
    this.name = 'InputError';
  }
}
