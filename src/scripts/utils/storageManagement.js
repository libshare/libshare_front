/**
 * Get logger title
 * @returns log button title
 */
export function loggerTitle(state) {
  state.isLoggedIn = localStorage && localStorage.getItem('token');
  return localStorage && localStorage.getItem('token') ? 'Log out' : 'Log in';
}
