/* eslint-disable no-unused-vars */

/**
 * Function which transform a value to template string
 *
 * This is mostly used to have html syntax highlighting on template strings (using extensions for lit-html).
 *
 * @param {string} value value to tranform to template
 * @returns a template string
 */
export function html(value) {
  return `${value}`;
}

/**
 * Toggle password visibility
 */
export function toggleVisibility() {
  const visibilityCheckbox = document.getElementById('password');
  if (visibilityCheckbox.type === 'password') {
    visibilityCheckbox.type = 'text';
  } else {
    visibilityCheckbox.type = 'password';
  }
}
