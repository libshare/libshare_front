/**
 * The loader handler
 *
 * @example
  const loader = new Loader();
  loader.fetchLoad(myUrl)
    .then(doSomething)
    .catch(doSomethingElse)
    .finally(doSomethingAgain);
 */
export default class Loader {
  constructor() {
    this.loader = document.createElement('div');
    this.loader.setAttribute('class', 'loader');
  }

  /**
   * Fetch your data and start a loader
   *
   * Clean the loader whith this.stopLoader();
   *
   * @param url defines the resource that you wish to fetch.
   * @param {*} options for fetch options
   * @see https://developer.mozilla.org/en-US/docs/Web/API/fetch#parameters for fetch parameters
   * @returns {Promise} the fetch Promise
   */
  async fetchLoad(url, options = {}) {
    const bodyElement = document.querySelector('body');
    bodyElement.insertBefore(
      this.loader,
      document.querySelector('div#app'),
    );
    try {
      const data = await fetch(url, options);
      if (data.headers.get('content-type').includes('json')) {
        return await data.json();
      } else {
        return await data.text();
      }
    } finally {
      this.stopLoader();
    }
  }

  /**
   * Clean all fetch loaders
   */
  stopLoader() {
    const bodyElement = document.querySelector('body');
    const loaders = document.querySelectorAll('.loader');
    loaders.forEach((element) => {
      bodyElement.removeChild(element);
    });
  }

  /**
   * Handle the parameter with the right action
   * will call a function or log any other object
   * @param {function|*} param a parameter to handle
   */
  action(param) {
    switch (typeof param) {
      case 'function':
        param();
        break;
      default:
        console.info(param);
        break;
    }
  }
}
