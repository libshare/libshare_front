/**
 * A minimal toaster to show notifications in application
 *
 * This toast have a progress bar for the delay of the notification.
 * Progress bar is visible, delay is 3 seconds and notification background
 * is transparent by default.
 *
 * Any notification can be removed by clicking on it.
 *
 * Notifications can not be stacked for now.
 *
 * @example
  const toast = new Toaster();
  toast.show('Hello'); // will show a toast with message 'Hello', for 3 seconds, with a progress bar and transparent background
  // or
  toast.show('Hello', { type: 'success', delay: 10000, progress: false });
  // toast with 'Hello' message, of 10 seconds with no progress bar and a background of success color
  // type => success is meant to be one of your css variables, here the css variable is `--success: green;`
  // you could call it whatever you want and define it as you need
 */
export default class Toaster {
  constructor() {
    this.INTERVAL_VALUE = 25;
    this.toaster = document.createElement('div');
    this.toaster.setAttribute('class', 'toaster');
    this.toaster.addEventListener('click', () => this.close());
    this.toaster.style =
      `border: 1px solid var(--text-color);padding:0.5em;margin:0.1em;
      cursor:pointer;width:10em;max-height:4em;position:absolute;top:0;right:0`;
    this.body = document.querySelector('body');
  }

  /**
   * Show a toast with a custom message
   *
   * @param {string} content toast message
   * @param {object} param
   * @param {boolean|string} param.type background color variable, default is boolean 'false' to set it to transparent,
   * it should so in your css you have configured a corresponding variable. For example, if you set `type: 'success'` you should
   * have a variable `--success: green;` in your css
   * @param {number|boolean} param.delay is the notification delay before it is removed, default delay is 3 seconds (3000ms),
   * you can set it to `false` if you don't want it to be removed automatically
   * @param {boolean} param.progress will show a progress bar corresponding to delay, progress is `true` by default. No progress
   * will be shown if you set delay to `false`
   */
  show(content, { type = false, delay = 3000, progress = true } = {}) {
    this.toaster.innerText = content || '';
    this.toaster.style.backgroundColor = type
      ? `var(--${type})`
      : 'transparent';
    this.body.insertBefore(
      this.toaster,
      document.querySelector('div#app'),
    );
    if (delay) {
      setTimeout(() => this.close(), delay);
      if (progress) {
        const progressBar = document.createElement('progress');
        progressBar.value = 0;
        progressBar.max = delay;
        progressBar.style =
          'position:absolute;bottom:0;left:0;width:100%;:background-color:var(--text-color);border:none;height:0.3em;';
        this.toaster.append(progressBar);
        const interval = setInterval(() => {
          progressBar.value += this.INTERVAL_VALUE;
          if (progressBar.value >= delay) {
            console.debug(`Clear interval ${interval}`);
            clearInterval(interval);
          }
        }, this.INTERVAL_VALUE);
      }
    }
  }

  /**
   * Close the Toaster
   */
  close() {
    this.body.removeChild(this.toaster);
  }
}
