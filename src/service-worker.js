const cacheName = 'libshare-v2.0.0';
const staticAssets = [
  '/styles/libshare.css',
  '/styles/vt-table.css',
  '/manifest/manifest.json',
  '/manifest/icon-192x192.png',
  '/manifest/icon-256x256.png',
  '/manifest/icon-384x384.png',
  '/manifest/icon-512x512.png',
];

self.addEventListener('install', async () => {
  const cache = await caches.open(cacheName);
  await cache.addAll(staticAssets);
});

self.addEventListener('fetch', (event) => {
  const req = event.request;
  if (/.*(authenticate)$/.test(req.url)) {
    event.respondWith(networkFirst(req));
  } else {
    event.respondWith(cacheFirst(req));
  }
});

async function cacheFirst(req) {
  const cache = await caches.open(cacheName);
  const cachedResponse = await cache.match(req);
  return cachedResponse || networkFirst(req);
}

async function networkFirst(req) {
  const cache = await caches.open(cacheName);
  try {
    const fresh = await fetch(req);
    cache.put(req, fresh.clone());
    return fresh;
  } catch (error) {
    console.error(error);
    const cachedResponse = await cache.match(req);
    return cachedResponse || fetch(req);
  }
}
