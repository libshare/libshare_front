globalThis.document = {};
globalThis._BASE_URL = '';
const importList = [];
for await (const path of Deno.readDir(`${Deno.cwd()}/src`)) {
  if (path.name != 'externals') {
    if (path.isDirectory) {
      await goIn(path.name);
    } else {
      if (isJavascript(path.name)) {
        importList.push(
          await import(`${Deno.cwd()}/src/${path.name}`),
        );
      }
    }
  }
}
async function goIn(folder) {
  for await (
    const path of Deno.readDir(`${Deno.cwd()}/src/${folder}`)
  ) {
    if (path.isDirectory) {
      await goIn(`${folder}/${path.name}`);
    } else {
      if (isJavascript(path.name)) {
        importList.push(
          await import(`${Deno.cwd()}/src/${folder}/${path.name}`),
        );
      }
    }
  }
}

/**
 * Check if the file is JavaScript or not
 *
 * @param {string} filename
 * @returns true if file is JavaScript
 */
function isJavascript(filename) {
  return filename.endsWith('.js');
}
