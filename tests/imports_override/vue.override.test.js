export function defineComponent() {}
export function createApp(_) {
  return {
    use: (a) => a,
    config: {
      globalProperties: {
        $store: {},
      },
    },
    mount: (a) => a,
  };
}
export function defineEmits() {
  return () => '';
}
export function defineProps() {
  return {
    data: {
      options: {},
    },
  };
}
