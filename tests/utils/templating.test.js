import { html } from '../../src/scripts/utils/templating.js';
import { assertEquals } from 'https://deno.land/std/testing/asserts.ts';

Deno.test('html template utility', () => {
  assertEquals(html('toto'), 'toto');
});
